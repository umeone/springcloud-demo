package com.wk.sc.sr.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * 服务调用
 *
 * @author zhang peng
 * @since 2019/4/8 16:35
 */
@Service
public class HelloService {

    @Autowired
    private RestTemplate restTemplate;

    /**
     * 服务调用
     *
     * @param name
     * @return
     */
    public String hiService(String name) {
        return restTemplate.getForObject("http://service-hi/hi?name=" + name, String.class); //service-hi大小写不敏感
    }

}
