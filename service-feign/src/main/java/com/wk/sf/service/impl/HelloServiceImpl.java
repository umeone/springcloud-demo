package com.wk.sf.service.impl;

import com.wk.sf.service.IHelloService;
import org.springframework.stereotype.Component;

/**
 * Feign 熔断服务降级实现
 *
 * @author zhang peng
 * @since 2019/4/8 19:11
 */
@Component
public class HelloServiceImpl implements IHelloService {
    @Override
    public String sayHi(String name) {
        return "sorry  " + name;
    }
}
