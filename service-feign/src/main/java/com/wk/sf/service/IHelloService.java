package com.wk.sf.service;

import com.wk.sf.service.impl.HelloServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * feign服务调用示例
 *
 * @author zhang peng
 * @since 2019/4/8 16:58
 */
@FeignClient(value = "service-hi",fallback = HelloServiceImpl.class)  //配置feign自带熔断器，fallback指定熔断后调用方法
public interface IHelloService {
    /**
     * 调用服务
     *
     * @param name
     * @return
     */
    @RequestMapping(value = "/hi", method = RequestMethod.GET)
    String sayHi(@RequestParam("name") String name);
}
