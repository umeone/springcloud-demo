package com.wk.sf.controller;

import com.wk.sf.service.IHelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * feign远程调用
 *
 * @author zhang peng
 * @since 2019/4/8 17:00
 */
@RestController
public class HiController {

    @Autowired
    private IHelloService helloService;

    @GetMapping(value = "/hi")
    public String sayHi(@RequestParam String name) {
        return helloService.sayHi(name);
    }

}
