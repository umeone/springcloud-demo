package com.wk.sc.csc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer  //开启服务注册
@SpringBootApplication
public class ConfigServerCluster1Application {

    public static void main(String[] args) {
        SpringApplication.run(ConfigServerCluster1Application.class, args);
    }

}
