package com.wk.sc.scs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient  //注意此处需要同时开启注册中心和配置服务
@EnableConfigServer
@SpringBootApplication
public class ConfigServerCluster2Application {

    public static void main(String[] args) {
        SpringApplication.run(ConfigServerCluster2Application.class, args);
    }

}
